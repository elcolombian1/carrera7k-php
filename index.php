<!--
	Author: W3layouts
	Author URL: http://w3layouts.com
	License: Creative Commons Attribution 3.0 Unported
	License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html lang="en">
<!-- Head -->
<head>
<title>Unillanos Corre</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="utf-8">
<meta name="keywords" content="Fit Club a Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />

	<link rel="shortcut icon" type="image/png" href="images/logo.png"/>
	<link href="css/easy-responsive-tabs.css" rel='stylesheet' type='text/css'/>
	<!-- //for Coaches css -->

	<!-- testimonials css -->
	<link rel="stylesheet" href="css/flexslider.css" type="text/css" media="screen" property="" /><!-- flexslider css -->
	<!-- //testimonials css -->

	<!-- default css files -->
	<link rel="stylesheet" href="css/bootstrap.css" type="text/css" media="all">
	<link rel="stylesheet" href="css/style.css" type="text/css" media="all">
	<link rel="stylesheet" href="css/font-awesome.min.css" />
	<!-- default css files -->
	
	<!--web font-->
	<link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet">
	<link href="//fonts.googleapis.com/css?family=PT+Sans+Caption:400,700&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">
	<link rel="stylesheet" href="css/style.css">

	<script src="https://www.gstatic.com/firebasejs/5.0.1/firebase.js"></script>
	<!--//web font-->
		
</head>
<script>
  // Initialize Firebase
  var config = {
    apiKey: "AIzaSyDuk2W67eXKn2Zz32b3cHny8l3XuixdiLE",
  authDomain: "carrera7k.firebaseapp.com",
  databaseURL: "https://carrera7k.firebaseio.com"
  };
  firebase.initializeApp(config);
  
</script>

<!-- Body -->
<body>




<div class="container-fluid loader text-center" id="loader">
	<img src="/images/cuete2.gif" class="img-responsive">
	<h1 style="color: white"><spam style="color:#e54848"><sttrong>Unillanos</sttrong></spam> Corre 7K...</h1>
	
</div>

<header> 
	<div class="conta-fluid border text-left" style="background-color: black;padding:1%;">
	<div class="container">
		<img src="/images/unillanos.png" height="6%" width="6%" class="img-responsive logo">
		<img src="/images/logo2.png" width="6%" height="6%" class="img-responsive logo">
		<a><span style="cursor: pointer;float:right;font-size: 300%;" id="ir" class="fa fa-facebook" style="font-size: 200%;"></span></a>
		<h3 style="float: right;color: white;padding: 2%;">18 de <spam style="color:#e54848">Noviembre</spam><strong> 2018 </strong></h3>	
		
		

	</div>				

		
</div>
</header>
<!-- banner -->
<br>
<div class=" banner">
	<div class="wthree-different-dot">
	<!-- header -->
		<div class="header">
			<div class="container">  
				<div class="header-mdl agileits-logo" style="background-color: rgba(0,0,0,0.9);"><!-- header-two --> 
					<h2 style="color:white">Unillanos <span style="color:#e54848">Corre 7K</span></h2> 
				</div>
				<div class="header-nav" id="header1"><!-- header-two --> 
					<nav class="navbar navbar-default">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button> 
						</div>



						<!-- top-nav -->
						<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
							<ul class="nav navbar-nav">
								<li><a href="#about" class="scroll" >Inscripción</a></li>
								<li><a href="#mapa2" class="scroll" data-hover="Home">Recorrido</a></li>
								<li><a href="#video" class="scroll" data-hover="About">Video Ruta</a></li> 
								<li><a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Reglamento <span class="caret"></span></a>
									<ul class="dropdown-menu">
										
										<li><a href="#participacion" class="scroll">PARTICIPACIÓN</a></li>
										<li><a href="#salida" class="scroll" >SALIDA  DE LA CARRERA</a></li>
										<li><a href="#horario" class="scroll">HORARIO DE SALIDA</a></li>
										<li><a href="#inscripciones" class="scroll" >INSCRIPCIONES</a></li>
										<li><a href="#entrega" class="scroll" >ENTREGA DE NÚMERO DE COMPETENCIA Y KIT</a></li>
										<li><a href="#resultados" class="scroll">RESULTADOS</a></li>
										<li><a href="#descalificaciones" class="scroll" >DESCALIFICACIONES</a></li>
										<li><a href="#premiacion" class="scroll" >PREMIACION</a></li>
										<li><a href="#categorias" class="scroll" >CATEGORIAS</a></li>
									</ul>
									<li><a href="#kit" class="scroll" data-hover="Testimonials">Kit del evento</a></li>	
								<li><a href="#pathner" class="scroll" data-hover="Testimonials">Patrocinadores</a></li>	

							</ul>  
							<div class="clearfix"> </div>	
						</div>
					</nav>    
				</div>	
			</div>	
		</div>
		<!-- //header --> 
		
		<!-- banner slider -->
		<div class="banner-top">
			<div class="slider">
				<div class="callbacks_container">
					<ul class="rslides callbacks callbacks1" id="slider4">
						<li>	
						<div class="wthree-different-dot">
							<div class="banner_text">
							<div class="container">
								<h2>PARTICIPACIÓN EN LA CARRERA UNILLANOS CORRE 7K</h2>
								<a href="#participacion" id="detalles">Detalles</a>
								<div class="thim-click-to-bottom">
									<a href="#about" class="scroll">
										<i class="fa  fa-long-arrow-down"></i>
									</a>
								</div>
							</div>
							</div>
						</div>
						</li>
						<li>
						<div class="wthree-different-dot">	
							<div class="banner_text">
							<div class="container">
								<h3>SALIDA  DE LA CARRERA 7 KM</h3>
								
								<a href="#salida" id="detalles">Detalles</a>
								<div class="thim-click-to-bottom">
									<a href="#about" class="scroll">
										<i class="fa  fa-long-arrow-down"></i>
									</a>
								</div>
							</div>
							</div>
							</div>
						</li>
						<li>	
						<div class="wthree-different-dot">
							<div class="banner_text">
							<div class="container">
								<h3>HORARIO DE SALIDA</h3>
								
								<a href="#horario" id="detalles">Detalles</a>
								<div class="thim-click-to-bottom">
									<a href="#about" class="scroll">
										<i class="fa  fa-long-arrow-down"></i>
									</a>
								</div>
							</div>
							</div>
							</div>
						</li>
						<li>	
						<div class="wthree-different-dot">
							<div class="banner_text">
							<div class="container">
								<h3>INSCRIPCIONES</h3>
								
								<a href="#inscripciones" id="detalles">Detalles</a>
								<div class="thim-click-to-bottom">
									<a href="#about" class="scroll">
										<i class="fa  fa-long-arrow-down"></i>
									</a>
								</div>
							</div>
							</div>
							</div>
						</li>
						<li>	
						<div class="wthree-different-dot">
							<div class="banner_text">
							<div class="container">
								<h3>ENTREGA DE NÚMERO DE COMPETENCIA Y KIT</h3>
								
								<a href="#entrega" id="detalles">Detalles</a>
								<div class="thim-click-to-bottom">
									<a href="#about" class="scroll">
										<i class="fa  fa-long-arrow-down"></i>
									</a>
								</div>
							</div>
							</div>
							</div>
						</li>
						<li>	
						<div class="wthree-different-dot">
							<div class="banner_text">
							<div class="container">
								<h3>RESULTADOS</h3>
								
								<a href="#resultados" id="detalles">Detalles</a>
								<div class="thim-click-to-bottom">
									<a href="#about" class="scroll">
										<i class="fa  fa-long-arrow-down"></i>
									</a>
								</div>
							</div>
							</div>
							</div>
						</li>
						<li>	
						<div class="wthree-different-dot">
							<div class="banner_text">
							<div class="container">
								<h3>DESCALIFICACIONES</h3>
								
								<a href="#descalificaciones" id="detalles">Detalles</a>
								<div class="thim-click-to-bottom">
									<a href="#about" class="scroll">
										<i class="fa  fa-long-arrow-down"></i>
									</a>
								</div>
							</div>
							</div>
							</div>
						</li>
						<li>	
						<div class="wthree-different-dot">
							<div class="banner_text">
							<div class="container">
								<h3>PREMIACION</h3>
								
								<a href="#premiacion" id="detalles">Detalles</a>
								<div class="thim-click-to-bottom">
									<a href="#about" class="scroll">
										<i class="fa  fa-long-arrow-down"></i>
									</a>
								</div>
							</div>
							</div>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- banner slider -->
		
	</div>
</div>
<!-- //banner -->


<!-- about -->	
<section class="about" id="about">
	<div class="container" style="background-color: rgba(0,0,0,0.8);padding: 2%;">
		<h3 class="heading">Unillanos Corre<strong> 7K</strong> <span style="text-decoration: none">Inscripción</span></h3>
		<div class="col-md-6 about-left">
			<form class="form" style="padding:5%;color:white" method="POST" action="inscrito.php" id="formulario">

				<label>Nombre Completo</label>
				<input class="form-control" type="text" name="nombre" placeholder="Ingresa tu nombre..." required>
				<hr>	
				<label>Edad</label>
				<input id="edad" class="form-control" type="number" min="0" max="99" name="edad" 	required>	
				<hr>	
				<label>Correo Electronico</label>
				<input class="form-control" type="email" min="0" max="99" name="correo" required>	
				<hr>	
				<label>Telefono</label>
				<input class="form-control" type="text" name="telefono" required>	
				<hr>
				<label>Genero</label>
				<select id="genero" class="form-control" oninput="cambio()" name="genero" required>
					<option></option>
					<option>Femenino</option>
					<option>Masculino</option>
				</select>		
				<hr>
				<label style="float-right">¿Eres estudiante o funcionario de la Universidad de los Llanos?</label>
				<select id="cargo" class="form-control" oninput="cambio()" name="cargo" required>
					<option></option>
					<option>No</option>
					<option>Estudiante</option>
					<option>Funcionario</option>
				</select>	
				<hr>
				<label id="estudiante1">Ingresa tu codigo estudiantil</label>
				<input id="estudiante2" type="text" class="form-control" name="codigo" placeholder="Codigo..." >
				<br>
				<label id="estudiante3">Carrera</label>

				<select id="estudiante-carrera" class="form-control" name="carrera">
					<option></option>
					<option>Ingeniería Agroindustrial</option>
					<option>Licenciatura en Educación Infantil</option>
					<option>Medicina Veterinaria y Zootecnia</option>
					<option>Ingeniería Agronómica</option>
					<option>Economía</option>
					<option>Biología</option>
					<option>Ingeniería de Sistemas</option>
					<option>Ingeniería Electrónica</option>
					<option>Administración de Empresas</option>
					<option>Contaduría Pública</option>
					<option>Enfermeria</option>
					<option>Licenciatura en Matemáticas</option>
					<option>Licenciatura en Educación Física y Deporte</option>
					<option>Mercadeo</option>
					<option>Funcionario</option>
				</select>
			<br>
			<label>Estoy de acuerdo con el <a href="#reglamento">reglamento</a> de la carrera</label>
			<input type="checkbox" name="" required="" oninvalid="this.setCustomValidity('Debes estar de acuerdo con el reglamento')" oninput="setCustomValidity('')">	
			<input type="submit" class="btn btn-danger btn-lg text-center" value="Inscibirme" >	 
		</form>

	</div>
	<div class="col-md-5 text-center" style="padding-top:10%">
		
		<h1  id="costo" style="color:white">Tu inscripcion tendra un costo de:<span style="font-weight: bold">   $15.500</span></h1>
		<h2  id="costofun" style="color:white">Tu inscripcion tendra un costo de:<span style="font-weight: bold">   $5000</span> <strong> Deberas cancelar dicho valor a la hora de reclamar el <a href="#kit">Kit</a></strong></h2>
		<hr class="cuenta">
								<h4 class="cuenta">Recuerda hacer tu consignación en un sucursal bancario Bancolombia</h4>
								<br>
								<h4 class="cuenta">Cuenta de ahorros: <strong> 364-000112-27</strong></h4>
								<br>
								<h4 class="cuenta">Enviar evidencia fotografica de consignación al <strong><span class="fa fa-whatsapp"></span> 3016350805 </strong>

		<h1  id="nocosto" style="color:white">Estas invitado a participar de manera <span style="font-weight: bold">Gratuita.</span></h1>
		<div id="tabla-cat">
		<h3 style="color:white;margin-top:10%;">Categoria</h3>
		<table class="table table-dark text-left" id="tabla">
			<tr>
				<td><strong>CATEGORIA</strong></td>
				<td><strong>EDAD</strong></td>
			</tr>
			<tr>
				<td id="categoria">UNVERSITARIOS UNILLANOS MASCULINO</td>
				<td id="edad">UNICA</td>
			</tr>
		</table>
	</div>
	</div>
</section>
<!-- about -->
<section class="mapa2" id="mapa2" style="background-color: black;padding:4%">
	<div class="container-fluid" id="mapa">
		<h1 class="text-center" style="color:white;padding:1%;">Recorrido</h1>
	<iframe src="https://www.google.com/maps/embed?pb=!1m32!1m12!1m3!1d5620.66934328798!2d-73.58718245341927!3d4.072915478614691!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m17!3e2!4m3!3m2!1d4.0718531!2d-73.58374359999999!4m3!3m2!1d4.0705551!2d-73.582425!4m3!3m2!1d4.0705652!2d-73.5823942!4m3!3m2!1d4.0718952999999996!2d-73.58371249999999!5e0!3m2!1ses!2sco!4v1537680823307" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
	</div>
</section>





<section class="container-fluid section text-center	" id="video">	
	<h1 style="	margin:2%;color:#e54848;font-weight:bold">Video del recorrido</h1>
				<div class="col-md-12 col-sm-12 col-xs-12">
					<iframe width="90%" height="450" src="https://www.youtube.com/embed/ryHxB1GgRqk" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
				</div>
	
</section>



<section class="container-fluid section text-center" id="reglamento">	
	
	<div id="dentro" class="container-fluid text-center">
		<h1> REGLAMENTO CARRERA UNILLANOS CORRE 7K</h1>
	<br>
	<br>
	<div class="col-md-12 col-xs-12 col-sm-12 text-left" id="participacion">
		<h4 id="regla">Articulo 1: Participación en la carrera unillanos corre 7k  <span class="fa fa-chevron-circle-down regla1 regla1"></spa3></h1>
		<hr>
		<div class="col-md-6 col-md-offset-3" id="regla1">	
			

			<p class="text-justify">Declaro de manera libre, espontánea y voluntaria que:</p>
			<br>

			<p>
				<span id="text">1.</span> He decidido participar en la prueba Unillanos corre 7k  que se realizará el día el 18 de Noviembre 2018 organizada por  la dependencia de bienestar institucional de la Universidad de los Llanos (en adelante, “los Organizadores”).
			</p>
				<br>
			<p class="text-justify">	<span id="text">2.</span> Que me encuentro en óptimas condiciones físicas, mentales y de salud, y que comprendo que debo estar entrenado para participar en la carrera y realizar este esfuerzo físico; así mismo, comprendo que la organización recomienda a todos los inscritos someterse a una estricta evaluación médica antes de la carrera. </p>
			
			<br>
			<p class="text-justify">	<span id="text">3.</span> Que no padezco ninguna enfermedad, lesión, incapacidad y/o condición que me inhabilite para participar en la Carrera ni que haga aconsejable no participar en la misma.</p>
			
			<br>
			<p class="text-justify">	<span id="text">4.</span> Que estoy enterado de las recomendaciones deportivas y médicas que debo adoptar para participar en la Carrera de manera adecuada, teniendo en cuenta mi condición física y circunstancias personales. </p>
			
			<br>
			<p class="text-justify">	<span id="text">5.</span> Asumo todos los riesgos asociados con mi participación en la Carrera incluyendo, pero no limitados a caídas y accidentes, enfermedades e incluso lesiones o fallecimiento, generadas entre otras razones, por mis antecedentes médicos o clínicos, por el contacto con los participantes, deshidratación, las consecuencias del clima tales como temperatura y/o humedad, tránsito vehicular y condiciones del camino, y en general todo el riesgo que declaro conocido y valorado por mí, en razón a que la actividad durante el desarrollo de la Carrera se encontrará bajo mi control y ejecución exclusiva como participante. </p>
			
			<br>
			<p class="text-justify">	<span id="text">6.</span> Habiendo leído esta declaración, conociendo los riesgos y considerando que los acepto por el hecho de participar en la carrera, yo, en mi nombre y en el de cualquier persona que actúe en mi representación, libero a los organizadores de la Carrera, a La Universidad De Los Llanos, y a todos los voluntarios, patrocinadores y/o representantes y sucesores, de todo reclamo o responsabilidad de cualquier tipo que surja en consecuencia de mi participación en este evento. </p>
			
			<br>
	 	<p class="text-justify">Así mismo exonero de responsabilidad a los anteriormente mencionados por cualquier extravío, robo y/o hurto que pudiese sufrir </p>

		</div>
</div>
	<div class="col-md-12 col-xs-12 col-sm-12 text-left" id="salida">

		<h4 id="regla">artículo 2: salida  de la carrera 7 km  <span class="fa fa-chevron-circle-down regla2"></span></h4>
		<hr>
		<div class="col-md-6 col-md-offset-3" id="regla2">
			<p>Recuerde que la salida de la carrera   tendrá lugar a las 8:15 am, Universidad De Los Llanos sede Barcelona.</p>
			<br>
			<p><strong> CONSULTE LA SALIDA POR COLORES - SECCIÓN-PACERS.</strong>  </p>



	</div>
	</div>
	<div class="col-md-12 col-xs-12 col-sm-12 text-left" id="horario">

		<h4 id="regla">artículo 3: horario de salida  <span class="fa fa-chevron-circle-down regla3"></span></h4>
		<hr>
		<div class="col-md-6 col-md-offset-3" id="regla3">
			<p>La hora de salida de la prueba será las 8:15 a.m.</p>



	</div>
	</div>




	<div class="col-md-12 col-xs-12 col-sm-12 text-left" id="inscripciones">

		<h4 id="regla">artículo 4: inscripciones  <span class="fa fa-chevron-circle-down regla4"></span></h4>
		<hr>
		<div class="col-md-6 col-md-offset-3" id="regla4">
			<p><strong>INSCRIPCIONES ORDINARIAS ABIERTA MASCULINA - FEMENINA</strong></p>
			<p>	1 de octubre al 16 de noviembre  de 2018</p>
			<p>	Valor: <strong>$15.500</strong></p>
			<p> Número de cuenta: 364-000112-27 Bancolombia  <strong>Cuenta de ahorros</strong></p>
				<br>
			<p><strong>	INSCRIPCIONES ORDINARIAS ABIERTA MASCULINA - FEMENINA</strong></p>
			<p> FUNCIONARIOS,ADMINISTRATIVOS O  DOCENTES</p>
			<p>	 Valor: <strong>$5.000</strong></p>

			<p><strong>Importante: </strong>Este valor deberá ser cancelado  en medio de pago en  efectivo el día de la entrega del kit. <strong>NOTA: </strong>El participante  Deberá presentar el carnet institucional para verificar su  información.</p>
				</p>
<br>
		<p><strong>INSCRIPCIONES ORDINARIAS CAT. UNIVERSITARIOS UNILLANOS</strong></p>
		<p>1 de octubre al 16 de noviembre   de 2018</p>
		<p>Valor: Gratis</p>
		<p>Inscripciones en línea en nuestro Fans Page</p>
		<p id="ir2"><a href="https://www.facebook.com/Unillanos-CORRE-7K-463837564108356/"><span class="fa fa-facebook"></span>  @unillanoscorre7k</a></p>
		<p><strong>Tambien puedes inscribirte en:</strong></p>
		<p>Página Web http://carrera7k.herokuapp.com/</p>
		<p><strong>Oficina de Bienestar instileftal</strong> </p>
<hr>
<p><strong>CONSIGNAR SOLO POR MEDIO DE CORRESPONSAL BANCARIO BANCOLOMBIA</strong></p>


	</div>
	</div>

	<div class="col-md-12 col-xs-12 col-sm-12 text-left" id="entrega">
		<h4 id="regla">articulo 5: entrega de número de competencia y kit  <span class="fa fa-chevron-circle-down regla5"></span></h4>
		<hr>
		<div class="col-md-6 col-md-offset-3" id="regla5">
			<p class="text-justify">
				Los números de competencia y los kits del atleta se entregarán en la Universidad De Los Llanos sede san Antonio de <strong>2:00pm a 6:00pm </strong>Dirigirse a la Universidad De Los Llanos sede san Antonio para reclamar el número de competencia y el kit es de carácter obligatorio ya que el día de la carrera no se entregarán números de competencia. Los horarios anteriores son las únicas instancias disponibles para esta actividad. </p>
				<br>
				<p class="text-justify">Recuerde que el número debe portarse en la parte delantera de su camiseta con los ganchos que se entregan para tal efleft
			</p>



	</div>
	</div>
	<div class="col-md-12 col-xs-12 col-sm-12 text-left" id="resultados">
	

		<h4 id="regla">artículo 6: resultados  <span class="fa fa-chevron-circle-down regla6"></span></h4>
		<hr>
		<div class="col-md-6 col-md-offset-3" id="regla6">
			<p class="text-justify">Los resultados oficiales de la carrera corresponderán únicamente a los informados por la organización de la carrera. Esta información será la única válida para efectos de premiación.</p>


		</div>
	</div>
	<div class="col-md-12 col-xs-12 col-sm-12 text-left" id="descalificaciones">
	

		<h4 id="regla">artículo 7: descalificaciones  <span class="fa fa-chevron-circle-down regla7"></span></h4>
		<hr>
		<div class="col-md-6 col-md-offset-3" id="regla7">
		<ul id="descalificados">
			<p class="text-justify">Serán descalificados todos los corredores que:</p>
			<br>
			<li><p class="text-justify ">No porten el número de competencia del 2018.</p></li>
			<li><p class="text-justify ">Alteren el número de competencia.</p></li>
			<li><p class="text-justify ">Cambien de número con otro atleta.</p></li>
			<li><p class="text-justify ">No respeten su horario de salida.</p></li>
			<li><p class="text-justify ">Se salten las vallas.</p></li>
			<li><p class="text-justify ">No respeten las indicaciones dadas por la organización.</p></li>
			<li><p class="text-justify ">Manifiesten mal estado físico.</p></li>
			<li><p class="text-justify ">Se presenten bajo el efecto del alcohol, sustancias psicoactivas.</p></li>
			<li><p class="text-justify ">Manifiesten un comportamiento no deportivo.</p></li>
		</ul>

		</div>
</div>
	<div class="col-md-12 col-xs-12 col-sm-12 text-left" id="premiacion">

		<h4 id="regla">artículo 8: premiacion  <span class="fa fa-chevron-circle-down regla8"></span></h4>
		<hr>
		<div class="col-md-6 col-md-offset-3" id="regla8">
				<p class="text-justify">Los premios se entregarán a los ganadores reconocidos por el juzgamiento de la organización del evento.</p>
				<br>
				<p class="text-justify ">Los premios serán entregados únicamente al ganador, quien debe, obligatoriamente, presentar su cédula de ciudadanía.</p>
				<hr>
				<table class="table text-center" style="padding: 4%">
					<thead>
						<tr>
							<td>
								<strong>Puesto</strong>
							</td>
							<td>
								<strong>Premio</strong>
							</td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>
								1°
							</td>
							<td>
								TROFEO, MEDALLA , KIT DE OBSEQUIOS
							</td>
						</tr>
						<tr>
							<td>
								2°
							</td>
							<td>
								MEDALLA , KIT DE OBSEQUIOS
							</td>
						</tr>
						<tr>
							<td>
								3°
							</td>
							<td>
								MEDALLA , KIT DE OBSEQUIOS
							</td>
						</tr>
						<tr>
							<td>
								4°
							</td>
							<td>
								MEDALLA , OBSEQUIO
							</td>
						</tr>
					</tbody>
				</table>
		</div>
	</div>

		<div class="col-md-12 col-xs-12 col-sm-12 text-left" id="categorias">

		<h4 id="regla">artículo 9: Categorias  <span class="fa fa-chevron-circle-down regla9"></span></h4>
		<hr>
		<div class="col-md-6 col-md-offset-3" id="regla9">
			<table class="table text-center">
				<tr>
					<td>
					<strong>CATEGORIA</strong>
					</td>
					<td>
						<strong> EDAD</strong>
					</td>
				</tr>
				<tr>
					<td>
						UNIVERSITARIOS UNILLANOS MASCULINO
					</td>
				
					<td>
						UNICA
					</td>
				</tr>
				<tr>
					<td>
						UNIVERSITARIOS UNILLANOS FEMENINA
					</td>
			
					<td>
						UNICA
					</td>
				</tr>
				<tr>
					<td>
						ABIERTA MASCULINO
					</td>
			
					<td>
						DE 15 AÑOS EN ADELANTE
					</td>
				</tr>
				<tr>
					<td>
						ABIERTA FEMENINO 
					</td>
			
					<td>
						DE 15 AÑOS EN ADELANTE
					</td>
				</tr>
			</table>
		

	</div>

</div>
	
</div>
</div>
</section>




<section class="kit" id="kit">
	<div class="container-fluid text-center" id="inkit">
		<h1>Kit del evento</h1>
		<hr>			
		<div class="col-md-12 text-left" >
			
			
			<ul id="kitli">
				<li><h4><span class="fa fa-check"></span><strong> Numero de participación</strong></h4></li>
				<li><h4><span class="fa fa-check"></span><strong> Medalla de finisher </strong></h4></li>
				<li><h4><span class="fa fa-check"></span><strong> Hidratación Isotónica</strong></h4></li>
				<li><h4><span class="fa fa-check"></span><strong> Obsequio de patrocinador</strong></h4></li>
			</ul>
			
			
		</div>
		
	</div>
	
</section>
<section class="patrocinadores" id="pathner">	
	<div class="container-fluid text-center" id="pain">	
			<h1 style="color:black">PATROCINADORES</h1>
			<hr>
			<img src="/images/cronosport.png" width="300" height="200" class="img-responsive">
			
			<img src="/images/running.png" width="300" height="200" class="img-responsive">
			<img src="/images/reto.png" width="300" height="200" class="img-responsive">
			<img src="/images/ISOTONIX.jpg" width="250" height="150" class="img-responsive">
			<img src="/images/tefai.jpg" width="300" height="200" class="img-responsive">
			<img src="/images/runners.png" width="300" height="200" class="img-responsive">
	</div>

</section>

	<!-- Default-JavaScript-File -->
	<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.js"></script>
	<!-- //Default-JavaScript-File -->

	<!-- Responsive tabs for coachhes section -->
	<script src="js/easy-responsive-tabs.js"></script>
	<script>
		$(document).ready(function () {
			$('#horizontalTab').easyResponsiveTabs({
			type: 'default', //Types: default, vertical, accordion           
			width: 'auto', //auto or any width like 600px
			fit: true,   // 100% fit in a container
			closed: 'accordion', // Start closed if in accordion view
			activate: function(event) { // Callback function if tab is switched
			var $tab = $(this);
			var $info = $('#tabInfo');
			var $name = $('span', $info);
			$name.text($tab.text());
			$info.show();
			}
			});
			$('#verticalTab').easyResponsiveTabs({
			type: 'vertical',
			width: 'auto',
			fit: true
			});
		});
	</script>
	<!-- //Responsive tabs for coachhes section -->

	<!-- scrolling script -->
	<script type="text/javascript">
		jQuery(document).ready(function($) {
			$(".scroll").click(function(event){		
				event.preventDefault();
				$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
			});
		});
	</script> 
	<!-- //scrolling script -->

	<!--banner Slider starts Here-->
	<script src="js/responsiveslides.min.js"></script>
	<script>
		// You can also use "$(window).load(function() {"
		$(function () {
		  // Slideshow 4
		  $("#slider4").responsiveSlides({
			auto: true,
			pager:true,
			nav:true,
			speed: 500,
			namespace: "callbacks",
			before: function () {
			  $('.events').append("<li>before event fired.</li>");
			},
			after: function () {
			  $('.events').append("<li>after event fired.</li>");
			}
		  });
	
		});
	 </script>
	<!--banner Slider ends Here-->
			
	<!-- Pop-up for pricing tables -->
	<script src="js/jquery.magnific-popup.js" type="text/javascript"></script>
		<script>
			$(document).ready(function() {
				$('.popup-with-zoom-anim').magnificPopup({
					type: 'inline',
					fixedContentPos: false,
					fixedBgPos: true,
					overflowY: 'auto',
					closeBtnInside: true,
					preloader: false,
					midClick: true,
					removalDelay: 300,
					mainClass: 'my-mfp-zoom-in'
			});
																							
		});
		</script>
	<!-- //Pop-up for pricing tables -->

	<!-- Stats-Number-Scroller-Animation-JavaScript -->
	<script src="js/waypoints.min.js"></script> 
	<script src="js/counterup.min.js"></script> 
	<script>
		jQuery(document).ready(function( $ ) {
			$('.counter').counterUp({
				delay: 10,
				time: 1000 
			});
		});


			$(window).load(function() {
			  $(".loader").fadeOut("slow");
			
						    $('.flexslider1').flexslider({
				    animation: "slide",
				    controlNav: "thumbnails"
				  });
			});


	</script>
	<!-- //Stats-Number-Scroller-Animation-JavaScript -->


	<!-- flexSlider --><!-- for testimonials -->
	<script defer src="js/jquery.flexslider.js"></script>
	<script type="text/javascript">
		$(window).load(function(){
		  $('.flexslider').flexslider({
			animation: "slide",
			start: function(slider){
			  $('body').removeClass('loading');
			}
		  });
		});
	</script>
	<!-- //flexSlider --><!-- for testimonials -->


	<!-- Smooth scrolling -->
	<script src="js/SmoothScroll.min.js"></script>
	<script type="text/javascript" src="js/move-top.js"></script>
	<script type="text/javascript" src="js/easing.js"></script>
	

	
	<!-- here stars scrolling icon -->
	<script type="text/javascript">
		$(document).ready(function() {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
			*/
								
			$().UItoTop({ easingType: 'easeOutQuart' });
								
			});

$(document).ready(function(){  
  $("a").click(function(e) {
    e.preventDefault();
    $('html, body').animate({
      scrollTop: $($.attr(this, 'href')).offset().top
    }, 1000);
  });
});
	</script>

	<!-- //here ends scrolling icon -->

	<script type="text/javascript" src="js/logica.js"></script>
</body>
<!-- //Body -->

</html>