<!--
	Author: W3layouts
	Author URL: http://w3layouts.com
	License: Creative Commons Attribution 3.0 Unported
	License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html lang="en">
<!-- Head -->
<head>
<title>Unillanos Corre</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="utf-8">
<meta name="keywords" content="Fit Club a Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />

	<link rel="shortcut icon" type="image/png" href="images/logo.png"/>
	<link href="css/easy-responsive-tabs.css" rel='stylesheet' type='text/css'/>
	<!-- //for Coaches css -->

	<!-- testimonials css -->
	<link rel="stylesheet" href="css/flexslider.css" type="text/css" media="screen" property="" /><!-- flexslider css -->
	<!-- //testimonials css -->

	<!-- default css files -->
	<link rel="stylesheet" href="css/bootstrap.css" type="text/css" media="all">
	<link rel="stylesheet" href="css/style.css" type="text/css" media="all">
	<link rel="stylesheet" href="css/font-awesome.min.css" />
	<!-- default css files -->
	
	<!--web font-->
	<link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet">
	<link href="//fonts.googleapis.com/css?family=PT+Sans+Caption:400,700&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">
	<link rel="stylesheet" href="css/style.css">
	<!--//web font-->
		
</head>

<!-- Body -->
<body>


<div class="container-fluid loader text-center" id="loader">
	<img src="/images/cuete2.gif" class="img-responsive">
	<h1 style="color: white"><spam style="color:white"><sttrong>Unillanos</sttrong></spam> Corre 7K...</h1>
	
</div>

<header> 
	<div class="conta-fluid border text-left" style="background-color: black;padding:1%;">
	<div class="container">
		<img src="/images/unillanos.png" height="6%" width="6%" class="img-responsive logo">
		<img src="/images/logo2.png" width="6%" height="6%" class="img-responsive logo">

		<a><span style="cursor: pointer;float:right;font-size: 300%;" id="ir" class="fa fa-facebook" style="font-size: 200%;"></span></a>
		

	</div>				

		
</div>
</header>
<!-- banner -->
<br>
<div class=" banner">
	<div class="wthree-different-dot">
	<!-- header -->
		<div class="header">
			<div class="container">  
				<div class="header-mdl agileits-logo" style="background-color: rgba(0,0,0,0.9);"><!-- header-two --> 
					<h2 style="color:white">Unillanos <span style="color:#e54848">Corre 7K</span></h2> 
				</div>
				<div class="header-nav" id="header1"><!-- header-two --> 
					<nav class="navbar navbar-default">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button> 
						</div>



						<!-- top-nav -->
						<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
							<ul class="nav navbar-nav" style="color:white">
								<h1 >Inscripción Completada, Te esperamos </h1>
								<hr>
								<h4>Recuerda hacer tu consignación en un sucursal bancario Bancolombia</h4>
								<br>
								<h3>Cuenta de ahorros: <strong> 364-000112-27</strong></h3>
								<br>
								<h4>Enviar evidencia fotografica de consignación al <strong><span class="fa fa-whatsapp"></span> 3016350805 </strong></h4>
								<hr>
								<form action="/" method="GET">
								<input type="submit" name="" class="btn btn-info btn-lg" value="Inicio">
								</form>
							</ul>  
							<div class="clearfix"> </div>	
						</div>
					</nav>    
				</div>	
			</div>	
		</div>
		<!-- //header --> 
		
		<!-- banner slider -->
		<div class="banner-top">
			<div class="slider">
				<div class="callbacks_container">
					<ul class="rslides callbacks callbacks1" id="slider4">
						<li>	
						<div class="wthree-different-dot">
							<div class="banner_text">
							<div class="container">
								
								
								<div class="thim-click-to-bottom">
									<a href="#about" class="scroll">
										<i class="fa  fa-long-arrow-down"></i>
									</a>
								</div>
							</div>
							</div>
						</div>
						</li>
						<li>
						<div class="wthree-different-dot">	
							<div class="banner_text">
							<div class="container">

								

								<div class="thim-click-to-bottom">
									<a href="#about" class="scroll">
										<i class="fa  fa-long-arrow-down"></i>
									</a>
								</div>
							</div>
							</div>
							</div>
						</li>
						<li>	
						<div class="wthree-different-dot">
							<div class="banner_text">
							<div class="container">
								
								
								
								<div class="thim-click-to-bottom">
									<a href="#about" class="scroll">
										<i class="fa  fa-long-arrow-down"></i>
									</a>
								</div>
							</div>
							</div>
							</div>
						</li>
						<li>	
						<div class="wthree-different-dot">
							<div class="banner_text">
							<div class="container">
								
								
								
								<div class="thim-click-to-bottom">
									<a href="#about" class="scroll">
										<i class="fa  fa-long-arrow-down"></i>
									</a>
								</div>
							</div>
							</div>
							</div>
						</li>
						<li>	
						<div class="wthree-different-dot">
							<div class="banner_text">
							<div class="container">
								
								
								
								<div class="thim-click-to-bottom">
									<a href="#about" class="scroll">
										<i class="fa  fa-long-arrow-down"></i>
									</a>
								</div>
							</div>
							</div>
							</div>
						</li>
						<li>	
						<div class="wthree-different-dot">
							<div class="banner_text">
							<div class="container">
								
								
								
								<div class="thim-click-to-bottom">
									<a href="#about" class="scroll">
										<i class="fa  fa-long-arrow-down"></i>
									</a>
								</div>
							</div>
							</div>
							</div>
						</li>
						<li>	
						<div class="wthree-different-dot">
							<div class="banner_text">
							<div class="container">
								
								
								
								<div class="thim-click-to-bottom">
									<a href="#about" class="scroll">
										<i class="fa  fa-long-arrow-down"></i>
									</a>
								</div>
							</div>
							</div>
							</div>
						</li>
						<li>	
						<div class="wthree-different-dot">
							<div class="banner_text">
							<div class="container">
								
								
								
								<div class="thim-click-to-bottom">
									<a href="#about" class="scroll">
										<i class="fa  fa-long-arrow-down"></i>
									</a>
								</div>
							</div>
							</div>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- banner slider -->
		
	</div>
</div>
<?php
			
			// $nombre = $_POST['nombre'];
			// $correo = $_POST['correo'];
			$nombre=$_POST['nombre'];
			$correo=$_POST['correo'];
			$telefono = $_POST['telefono'];
		    $genero = $_POST['genero'];
		    $edad = $_POST['edad'];
		    $codigo = $_POST['codigo'];
		    $carrera = $_POST['carrera'];
		    $cargo = $_POST['cargo'];
		    $costo = 0;
		    $categoria="";

	
	if($cargo=="Estudiante" && $genero=="Masculino"){
		$categoria="UNIVERSITARIOS UNILLANOS MASCULINO";
	}
	if($cargo=="Estudiante" && $genero=="Femenino"){
		$categoria="UNIVERSITARIOS UNILLANOS FEMENINO";
	}

	if($cargo=="No" && $genero=="Masculino"){
		$categoria="ABIERTA MASCULINO";
        $costo=15500;
	}
	if($cargo=="No" && $genero=="Femenino"){
		$categoria="ABIERTA FEMENINO";
        $costo=15500;
	}

    if($cargo=="Funcionario" && $genero=="Masculino"){
        $categoria="ABIERTA FUNCIONARIOS MASCULINO";
        $costo=5000;
    }
    if($cargo=="Funcionario" && $genero=="Femenino"){
        $categoria="ABIERTA FUNCIONARIOS FEMENINO";
        $costo=5000;
    }
					

			
			
			$correo1='wilder.herrera@unillanos.edu.co';
			
			require ('vendor/phpmailer/phpmailer/PHPMailerAutoload.php');
			$mail = new PHPMailer();
		 	$mail2= new PHPMailer();
		 	

		 	$mail->debug=4;
			$mail->IsSMTP();                      	                // Set mailer to use SMTP
			$mail->Host = 'smtp.gmail.com';                 // Specify main and backup server
			$mail->Port = 587;                                    // Set the SMTP port
			$mail->SMTPAuth = true;                               // Enable SMTP authentication
			$mail->Username = 'unillanoscorre@gmail.com';                // SMTP username
			$mail->Password = '1994rivera';                  // SMTP password
			$mail->SMTPSecure = 'tls';     


			                           // Enable encryption, 'ssl' also accepted
			$mail->From ='unillanoscorre@gmail.com';
			$mail->FromName = 'Inscrito a la carrera Unillanos 7K';
			$mail->AddAddress('unillanoscorre@gmail.com');
			$mail->IsHTML(true);                                  // Set email format to HTML
			$mail->Subject = 'Nuevo participante';
			$mail->Body    = '	<!DOCTYPE html>
									<html>
									<head>	
									<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
									  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">

									  <link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet">
									</head>	
									<body>	
										<h1>Nuevo Corredor</h1>
										        <hr>
										        <table>
										        <tr>
										        <td><strong>Nombre: </strong>	'.$nombre .'</td>
										        </tr>

										        <tr>
										        <td><strong>Edad: </strong>	'.$edad .'</td>
										        </tr>
										        <tr>
										        <td><strong>Correo: </strong>	'.$correo .'</td>
										        </tr>
										        <tr>
										        <td><strong>Telefono: </strong>	'.$telefono .'</td>
										        </tr>
										        <tr>
										        <td><strong>Genero: </strong>	'.$genero .'</td>
										        </tr>
										        <tr>
										        <td><strong>Carrera: </strong>	'.$carrera.'</td>
										        </tr>
										        <tr>
										        <td><strong>Codigo: </strong>	'.$codigo.'</td>
										        </tr>
										        <tr>
										        <td><strong>Costo: </strong>    '.$costo.'</td>
										        </tr>
										        <tr>
										        <td><strong>Categoria: </strong>'.$categoria.'</td>
										        </tr>
										        </table>  				   
								   	<body>
								   	<html>
								   	';
			
			if(!$mail->Send()) {
			   echo 'No se pudo enviar el mensaje';
			   echo 'Mailer Error: ' . $mail->ErrorInfo;
			   exit;
			}
			

		 	$mail2->debug=4;
			$mail2->IsSMTP();                      	                // Set mailer to use SMTP
			$mail2->Host = 'smtp.gmail.com';                 // Specify main and backup server
			$mail2->Port = 587;                                    // Set the SMTP port
			$mail2->SMTPAuth = true;                               // Enable SMTP authentication
			$mail2->Username = 'unillanoscorre@gmail.com';                // SMTP username
			$mail2->Password = '1994rivera';                  // SMTP password
			$mail2->SMTPSecure = 'tls';     

			
			                           // Enable encryption, 'ssl' also accepted
			$mail2->From ='unillanoscorre@gmail.com';
			$mail2->FromName = 'Inscrito a la carrera Unillanos 7K';
			$mail2->AddAddress($correo);
			$mail2->IsHTML(true);                                  // Set email format to HTML
			$mail2->Subject = "Ya estas inscrito para participar Unillanos Corre 7K";
			echo("console.log(".$cargo.")");
			if($cargo=="Funcionario"){
				$mail2->Body    = ( '	<!DOCTYPE html>
									<html>
									<head>	
									<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
									  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">

									  <link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet">
									</head>	
									<body>	
										 <h1 style="color:white;background-color:black;padding:2%">Hola '.$nombre.', Gracias por inscribirte te esperamos, <span style="color:#e54848">Unillanos Corre 7k</span></h1>  
										 <h1 style="color:white;background-color:black;padding:2%">Tu inscripcion tendra un costo de: $5000 deberas cancelar dicho valor a la hora de reclamar el kit</h1>
								   	<body>
								   	<html>
								   	' );}

				if($cargo=="No"){
				$mail2->Body    = ( '	<!DOCTYPE html>
									<html>
									<head>	
									<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
									  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">

									  <link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet">
									</head>	
									<body>	
										 <h1 style="color:white;background-color:black;padding:2%">Hola '.$nombre.', Gracias por inscribirte te esperamos, <span style="color:#e54848">Unillanos Corre 7k</span></h1>  
										 <h1 style="color:white;background-color:black;padding:2%"> Realiza el pago de $'.$costo.' a traves de una consignación en la cuenta de ahorros numero : <span style="color:#e54848">364-000112-27</span> de Bancolombia y envia la evidencia fotografica de tu pago al <span style="color:#e54848">3016350805</span></h1>
								   	<body>
								   	<html>
								   	' );
			}
				if($cargo=="Estudiante"){

						$mail2->Body    = ( '	<!DOCTYPE html>
									<html>
									<head>	
									<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
									  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">

									  <link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet">
									</head>	
									<body>	
										 <h1 style="color:white;background-color:black;padding:2%">Hola '.$nombre.', Gracias por inscribirte te esperamos, <span style="color:#e54848">Unillanos Corre 7k</span></h1>  
										
								   	<body>
								   	<html>
								   	' );
				}
			
			if(!$mail2->Send()) {
			   echo 'No se pudo enviar el mensaje';
			   echo 'Mailer Error: ' . $mail2->ErrorInfo;
			   exit;
			}
			
?>


	<!-- Default-JavaScript-File -->
	<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.js"></script>
	<!-- //Default-JavaScript-File -->

	<!-- Responsive tabs for coachhes section -->
	<script src="js/easy-responsive-tabs.js"></script>
	<script>
		$(document).ready(function () {
			$('#horizontalTab').easyResponsiveTabs({
			type: 'default', //Types: default, vertical, accordion           
			width: 'auto', //auto or any width like 600px
			fit: true,   // 100% fit in a container
			closed: 'accordion', // Start closed if in accordion view
			activate: function(event) { // Callback function if tab is switched
			var $tab = $(this);
			var $info = $('#tabInfo');
			var $name = $('span', $info);
			$name.text($tab.text());
			$info.show();
			}
			});
			$('#verticalTab').easyResponsiveTabs({
			type: 'vertical',
			width: 'auto',
			fit: true
			});
		});
	</script>
	<!-- //Responsive tabs for coachhes section -->

	<!-- scrolling script -->
	<script type="text/javascript">
		jQuery(document).ready(function($) {
			$(".scroll").click(function(event){		
				event.preventDefault();
				$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
			});
		});
	</script> 
	<!-- //scrolling script -->

	<!--banner Slider starts Here-->
	<script src="js/responsiveslides.min.js"></script>
	<script>
		// You can also use "$(window).load(function() {"
		$(function () {
		  // Slideshow 4
		  $("#slider4").responsiveSlides({
			auto: true,
			pager:true,
			nav:true,
			speed: 500,
			namespace: "callbacks",
			before: function () {
			  $('.events').append("<li>before event fired.</li>");
			},
			after: function () {
			  $('.events').append("<li>after event fired.</li>");
			}
		  });
	
		});
	 </script>
	<!--banner Slider ends Here-->
			
	<!-- Pop-up for pricing tables -->
	<script src="js/jquery.magnific-popup.js" type="text/javascript"></script>
		<script>
			$(document).ready(function() {
				$('.popup-with-zoom-anim').magnificPopup({
					type: 'inline',
					fixedContentPos: false,
					fixedBgPos: true,
					overflowY: 'auto',
					closeBtnInside: true,
					preloader: false,
					midClick: true,
					removalDelay: 300,
					mainClass: 'my-mfp-zoom-in'
			});
																							
		});
		</script>
	<!-- //Pop-up for pricing tables -->

	<!-- Stats-Number-Scroller-Animation-JavaScript -->
	<script src="js/waypoints.min.js"></script> 
	<script src="js/counterup.min.js"></script> 
	<script>
		jQuery(document).ready(function( $ ) {
			$('.counter').counterUp({
				delay: 10,
				time: 1000 
			});
		});


			$(window).load(function() {
			  $(".loader").fadeOut("slow");
			
						    $('.flexslider1').flexslider({
				    animation: "slide",
				    controlNav: "thumbnails"
				  });
			});


	</script>
	<!-- //Stats-Number-Scroller-Animation-JavaScript -->


	<!-- flexSlider --><!-- for testimonials -->
	<script defer src="js/jquery.flexslider.js"></script>
	<script type="text/javascript">
		$(window).load(function(){
		  $('.flexslider').flexslider({
			animation: "slide",
			start: function(slider){
			  $('body').removeClass('loading');
			}
		  });
		});
	</script>
	<!-- //flexSlider --><!-- for testimonials -->


	<!-- Smooth scrolling -->
	<script src="js/SmoothScroll.min.js"></script>
	<script type="text/javascript" src="js/move-top.js"></script>
	<script type="text/javascript" src="js/easing.js"></script>
	

	
	<!-- here stars scrolling icon -->
	<script type="text/javascript">
		$(document).ready(function() {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
			*/
								
			$().UItoTop({ easingType: 'easeOutQuart' });
								
			});

$(document).ready(function(){  
  $("a").click(function(e) {
    e.preventDefault();
    $('html, body').animate({
      scrollTop: $($.attr(this, 'href')).offset().top
    }, 1000);
  });
});
	</script>

	<!-- //here ends scrolling icon -->

	<script type="text/javascript" src="js/logica.js"></script>
</body>
<!-- //Body -->

</html>