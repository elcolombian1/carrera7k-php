$("#estudiante1").hide();
$("#estudiante2").hide();
$("#estudiante3").hide();
$("#estudiante-carrera").hide();
$("#costo").hide(500);
$("#nocosto").hide(500);
$("#costofun").hide(500);
$(".cuenta").hide(500);

var database=firebase.database();
(function ($) {
    $.fn.serializeFormJSON = function () {

        var o = {};
        var a = this.serializeArray();
        $.each(a, function () {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
})(jQuery);

$("#formulario").submit(function(e) {
    
  	var data = $(this).serializeFormJSON();
  	var costo=0;
	var categoria="";

	if(data.cargo=="Estudiante" && data.genero=="Masculino"){
		categoria="UNIVERSITARIOS UNILLANOS MASCULINO"
	}
	if(data.cargo=="Estudiante" && data.genero=="Femenino"){
		categoria="UNIVERSITARIOS UNILLANOS FEMENINO"
	}

	if(data.cargo=="No" && data.genero=="Masculino"){
		categoria="ABIERTA MASCULINO";
        costo=15500;
	}
	if(data.cargo=="No" && data.genero=="Femenino"){
		categoria="ABIERTA FEMENINO";
        costo=15500;
	}

    if(data.cargo=="Funcionario" && data.genero=="Masculino"){
        categoria="ABIERTA FUNCIONARIOS MASCULINO";
        costo=5000;
    }
    if(data.cargo=="Funcionario" && data.genero=="Femenino"){
        categoria="ABIERTA FUNCIONARIOS FEMENINO";
        costo=5000;
    }
 console.log(costo)
	var data2={

        Nombre:data.nombre,
        Edad:data.edad,
        Correo:data.correo,
        Telefono:data.telefono,
        Genero:data.genero,
        Codigo:data.codigo,
        Carrera:data.carrera,
        Costo:costo,
        Categoria:categoria
}
    
    database.ref("participantes/").push(data2);
   
});

function cambio(){
var gen=$("#genero").val();	
console.log($("#cargo").val());

if($("#cargo").val()=="Estudiante"){


	$("#estudiante1").show(500);
	$("#costo").hide(500);
	$(".cuenta").hide(500);
	$("#nocosto").show(500);
	$("#costofun").hide(500);
$("#estudiante2").show(500);
$("#estudiante3").show(500);
$("#estudiante-carrera").show(500);		




if(gen=="Masculino"){
	$("#tabla-cat").show(300);
	$("#categoria").html("UNIVERSITARIOS UNILLANOS MASCULINO");
	$("#edad").html("UNICA")}

if(gen=="Femenino"){
	$("#tabla-cat").show(300);
	$("#categoria").html("UNIVERSITARIOS UNILLANOS FEMENINO");
	$("#edad").html("UNICA")}


}

else if($("#cargo").val()=="Funcionario"){


	$("#estudiante1").hide(500);
	$("#costo").hide(500);
	$("#costofun").show(500);
	$(".cuenta").hide(500);
	$("#nocosto").hide(500);
$("#estudiante2").hide(500);
$("#estudiante3").hide(500);
$("#estudiante-carrera").hide(500);		




if(gen=="Masculino"){
	$("#tabla-cat").show(300);
	$("#categoria").html("FUNCIONARIOS UNILLANOS MASCULINO");
	$("#edad").html("UNICA")}

if(gen=="Femenino"){
	$("#tabla-cat").show(300);
	$("#categoria").html("FUNCIONARIOS UNILLANOS FEMENINO");
	$("#edad").html("UNICA")}


}

else{
	$("#estudiante1").hide(500);
$("#estudiante2").hide(500);
$("#costofun").hide(500);
$("#estudiante3").hide(500);
$("#estudiante-carrera").hide(500);	
$("#costo").show(500);
$(".cuenta").show(500);
$("#nocosto").hide(500);

if(gen=="Femenino"){
	$("#tabla-cat").show(300);
	$("#categoria").html("ABIERTA FEMENINA");
	$("#edad").html("DE 15 AÑOS EN ADELANTE")}

if(gen=="Masculino"){
	$("#tabla-cat").show(300);
	$("#categoria").html("ABIERTA MASCULINA");
	$("#edad").html("DE 15 AÑOS EN ADELANTE")}
}
	
}
	
$("#regla1").hide(300);
$("#regla2").hide(300);
$("#regla3").hide(300);
$("#regla4").hide(300);
$("#regla5").hide(300);
$("#regla6").hide(300);
$("#regla7").hide(300);
$("#regla8").hide(300);
$("#regla9").hide(300);

var regla1=false;
var regla2=false;
var regla3=false;
var regla4=false;
var regla5=false;
var regla6=false;
var regla7=false;
var regla8=false;
var regla9=false;



$(".regla1").click(()=>{
	if(!regla1)
	{
	$(".regla1").removeClass("fa-chevron-circle-down");
	$(".regla1").addClass("fa-chevron-circle-up");
	$("#regla1").show(300);
	regla1=!regla1
}


	else{

		$(".regla1").addClass("fa-chevron-circle-down");
		$(".regla1").removeClass("fa-chevron-circle-up");
		$("#regla1").hide(300);
		regla1=!regla1
	}

});

$(".regla2").click(()=>{
	if(!regla2)
	{
	$(".regla2").removeClass("fa-chevron-circle-down");
	$(".regla2").addClass("fa-chevron-circle-up");
	$("#regla2").show(300);
	regla2=!regla2
}


	else{

		$(".regla2").addClass("fa-chevron-circle-down");
		$(".regla2").removeClass("fa-chevron-circle-up");
		$("#regla2").hide(300);
		regla2=!regla2
	}

});
$(".regla3").click(()=>{
	if(!regla3)
	{
	$(".regla3").removeClass("fa-chevron-circle-down");
	$(".regla3").addClass("fa-chevron-circle-up");
	$("#regla3").show(300);
	regla3=!regla3
}


	else{

		$(".regla3").addClass("fa-chevron-circle-down");
		$(".regla3").removeClass("fa-chevron-circle-up");
		$("#regla3").hide(300);
		regla3=!regla3
	}

});
$(".regla4").click(()=>{
if(!regla4)
	{
	$(".regla4").removeClass("fa-chevron-circle-down");
	$(".regla4").addClass("fa-chevron-circle-up");
	$("#regla4").show(300);
	regla4=!regla4
}


	else{

		$(".regla4").addClass("fa-chevron-circle-down");
		$(".regla4").removeClass("fa-chevron-circle-up");
		$("#regla4").hide(300);
		regla4=!regla4
	}

});
$(".regla5").click(()=>{
if(!regla5)
	{
	$(".regla5").removeClass("fa-chevron-circle-down");
	$(".regla5").addClass("fa-chevron-circle-up");
	$("#regla5").show(300);
	regla5=!regla5
}


	else{

		$(".regla5").addClass("fa-chevron-circle-down");
		$(".regla5").removeClass("fa-chevron-circle-up");
		$("#regla5").hide(300);
		regla5=!regla5
	}

});
$(".regla6").click(()=>{
if(!regla6)
	{
	$(".regla6").removeClass("fa-chevron-circle-down");
	$(".regla6").addClass("fa-chevron-circle-up");
	$("#regla6").show(300);
	regla6=!regla6
}


	else{

		$(".regla6").addClass("fa-chevron-circle-down");
		$(".regla6").removeClass("fa-chevron-circle-up");
		$("#regla6").hide(300);
		regla6=!regla6
	}

});
$(".regla7").click(()=>{
if(!regla7)
	{
	$(".regla7").removeClass("fa-chevron-circle-down");
	$(".regla7").addClass("fa-chevron-circle-up");
	$("#regla7").show(300);
	regla7=!regla7
}


	else{

		$(".regla7").addClass("fa-chevron-circle-down");
		$(".regla7").removeClass("fa-chevron-circle-up");
		$("#regla7").hide(300);
		regla7=!regla7
	}

});
$(".regla8").click(()=>{
if(!regla8)
	{
	$(".regla8").removeClass("fa-chevron-circle-down");
	$(".regla8").addClass("fa-chevron-circle-up");
	$("#regla8").show(300);
	regla8=!regla8
}


	else{

		$(".regla8").addClass("fa-chevron-circle-down");
		$(".regla8").removeClass("fa-chevron-circle-up");
		$("#regla8").hide(300);
		regla8=!regla8
	}

});
$(".regla9").click(()=>{
if(!regla9)
	{
	$(".regla9").removeClass("fa-chevron-circle-down");
	$(".regla9").addClass("fa-chevron-circle-up");
	$("#regla9").show(300);
	regla9=!regla9
}


	else{

		$(".regla9").addClass("fa-chevron-circle-down");
		$(".regla9").removeClass("fa-chevron-circle-up");
		$("#regla9").hide(300);
		regla9=!regla9
	}

});
function facebook(){
			  location.href=("https://www.facebook.com/Unillanos-CORRE-7K-463837564108356/");
}
$("#ir2").click(()=>{


	facebook();
	
		}

	)

$("#ir").click(()=>{


	facebook();
	
		}

	)


$("#tabla-cat").hide();



